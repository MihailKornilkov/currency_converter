package com.example.currencyconverter.data

import com.example.currencyconverter.data.models.CurrencyResponce
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("/latest")
    suspend fun getRates(
        @Query("base") base : String
    ) : Response<CurrencyResponce>
}