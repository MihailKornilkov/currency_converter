package com.example.currencyconverter

import android.app.Application
import dagger.internal.DaggerGenerated

@DaggerGenerated
class CurrencyApplication : Application() {

}
