package com.example.currencyconverter.main

import com.example.currencyconverter.data.CurrencyApi
import com.example.currencyconverter.data.models.CurrencyResponce
import com.example.currencyconverter.util.Resource
import java.lang.Exception
import javax.inject.Inject

class DefaultMainRepository @Inject constructor(
    private val api: CurrencyApi
) : MainRepository{

    override suspend fun getRates(base: String): Resource<CurrencyResponce> {
        return try {
            val responce = api.getRates(base)
            val result = responce.body()
            if(responce.isSuccessful && result != null) {
                Resource.Success(result)
            } else {
                Resource.Error(responce.message())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }
}