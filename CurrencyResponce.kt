package com.example.currencyconverter.data.models

data class CurrencyResponce(
    val amount: Int,
    val base: String,
    val date: String,
    val rates: Rates
)